/**
 * App module
 *
 * @author Milos Jovanovic
 */
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';

// COMPONENTS
import {AppComponent} from './app.component';
import {HeaderComponent} from '../components/header/header.component';
import {DashboardComponent} from '../components/dashboard/dashboard.component';
import {MenuComponent} from '../components/menu/menu.component';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        DashboardComponent,
        MenuComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
