/**
 * Dashboard component
 *
 * Contains template and logic of dashboard
 *
 * @author Milos Jovanovic
 */

import {Component} from '@angular/core';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

}
