/**
 * Header component
 *
 * Contains template and logic of header
 *
 * @author Milos Jovanovic
 */

import {Component, Input} from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
}
